package homework3_1.task3;

import homework3_1.task2.Student;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Alex", "Ivanov");
        student1.setGrades(new int[]{5, 5, 4, 5, 3, 5, 5, 4, 3, 3});
        Student student2 = new Student("Ivan", "Antonov");
        student2.setGrades(new int[]{5, 3, 4, 5, 4, 5, 4, 4, 4, 3});
        Student student3 = new Student("Katya", "Fedorova");
        student3.setGrades(new int[]{5, 2, 3, 5, 4, 3, 4, 4, 0, 0});

        Student best = StudentService.bestStudent(new Student[]{student1, student2, student3});

        System.out.println(best.getSurname());
        Student[] mass = new Student[]{student1, student2, student3};

        StudentService.sortBySurname(mass);

        System.out.println(mass[0].getSurname());
        System.out.println(mass[1].getSurname());
        System.out.println(mass[2].getSurname());

    }
}

