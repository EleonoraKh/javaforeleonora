package professional.homework1.homework1_1;

import java.io.FileNotFoundException;

public class MyCheckedException extends FileNotFoundException {
    public MyCheckedException(){
        super();
    }
    public MyCheckedException(String message){
        super(message);
    }
}
