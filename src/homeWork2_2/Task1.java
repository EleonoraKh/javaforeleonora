package homeWork2_2;
/*На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.*/

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        //заполняем массив
        int [][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        // подсчет
        int min = 0;//минимальное значение
        for (int i =0; i < m; i++){
           min = a[i][0];
             for (int j = 1; j < n; j++){
                 if (min > a[i][j]) {
                     min = a[i][j];
                 }

            }
            System.out.print(min+ " ");

        }

        }

    }



