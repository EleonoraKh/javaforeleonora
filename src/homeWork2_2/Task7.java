package homeWork2_2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] name = new String[n];
        //заполнили массив n
        for (int i = 0; i < n; i++) {
            name[i] = scanner.next();
        }

        String[] sobach = new String[n];
        //заполнили массив n
        for (int i = 0; i < n; i++) {
            sobach[i] = scanner.next();
        }

        double[][] ocenka = new double[n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                ocenka[i][j] = scanner.nextDouble();
            }
        }
        double[] srednee = new double[n];
        double temp = 0;
        for (int i = 0; i < n; i++) {
            temp = 0;
            for (int j = 0; j < 3; j++) {
                temp += ocenka[i][j];
            }
            // находим среднее арифм , нужно перевести в десят, поэтому умножаем на 10 и делим на 10,0
            srednee[i] = ((int) (temp / 3 * 10)) / 10.0;

        }
        //найти индекс максимальной средней оценки
        int index1 = 0;
        int index2 = 0;
        int index3 = 0;
        temp = 0;
        for (int i = 0; i < srednee.length; i++) {
            if (temp < srednee[i]) {
                temp = srednee[i];
                index1 = i;
            }
        }
        temp = 0;
        for (int i = 0; i < srednee.length; i++) {
            if ((srednee[index1] != srednee[i]) && temp < srednee[i]) {
                temp = srednee[i];
                index2 = i;
            }

        }
        temp = 0;
        for (int i = 0; i < srednee.length; i++) {
            if ((srednee[index1] != srednee[i]) && (srednee[index2] != srednee[i]) && temp < srednee[i]) {
                temp = srednee[i];
                index3 = i;
            }
        }
        System.out.println(name[index1] + ": " + sobach[index1] + ", " + srednee[index1]);
        System.out.println(name[index2] + ": " + sobach[index2] + ", " + srednee[index2]);
        System.out.println(name[index3] + ": " + sobach[index3] + ", " + srednee[index3]);

    }
}

