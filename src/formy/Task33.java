package formy;

public class Task33 {
    public static void main(String[] args) {
        A a = new A("5");
        a.print();
    }
}
class A{
    String s;

    A(String s) {
        this.s = s;
    }

    void print(){
        System.out.println(s);
    }
}
