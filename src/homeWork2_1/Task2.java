package homeWork2_1;

import java.util.Scanner;

/*На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.*/
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a1 = new int[n];
        //заполняем наш массив входными данными
        for (int i = 0; i < n; i++) {
            a1[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] a2 = new int[m];
        for (int j = 0; j < m; j++) {
            a2[j] = scanner.nextInt();
        }
        System.out.println(java.util.Arrays.equals(a1, a2));
    }
}
