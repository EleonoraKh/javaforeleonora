package homework3_3.Task4;

import java.util.ArrayList;
import java.util.List;

public class Dog {
    private List<Integer> grades = new ArrayList<>();
    private String nick;

    public Dog(String nick) {
        setNick(nick);
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setGrades(int grade) {
        grades.add(grade);
    }

    public double averageGrade() {
        double sum = 0;
        for (int i = 0; i < grades.size(); i++) {
            sum += grades.get(i);
        }
        return (int)(sum / grades.size() * 10) / 10.0;
    }
}
