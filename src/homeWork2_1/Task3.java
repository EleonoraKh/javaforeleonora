package homeWork2_1;

import java.util.Scanner;

import static java.util.Arrays.binarySearch;

/*На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.
Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего.*/
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < ai.length; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        for (int j = 0; j < ai.length; j++) {
            if (m < ai[j]) {
                System.out.println(j);
                break;
            }
        }
    }
}




