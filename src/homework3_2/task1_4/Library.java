package homework3_2.task1_4;

import java.util.ArrayList;

public class Library {
    private ArrayList<Book> books = new ArrayList<>();
    private ArrayList<Visitor> visitors = new ArrayList<>();

    public void addBook(Book input) {
        //если список книг (не)содержит новую книгу
        for (int i = 0; i < books.size(); i++) {
            if((books.get(i)).getTitle().equals(input.getTitle())){
                System.out.println("Такая книга уже есть");
                return;
            }
        }
        books.add(input);
    }

    public void deleteBook(String title){
        for (int i = 0; i < books.size(); i++) {
            if((books.get(i)).getTitle().equals(title)){
                books.remove(i);
                return;
            }
        }
        System.out.println("Такой книги нет");
    }

    public Book getBook(String title){
        for (int i = 0; i < books.size(); i++) {
            if((books.get(i)).getTitle().equals(title)){
                return books.get(i);
            }
        }
        System.out.println("Такой книги нет");
        return null;
    }

    public ArrayList<Book> getBookList(String author){
        ArrayList<Book> booksByAuthor = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            if((books.get(i)).getAuthor().equals(author)){
                booksByAuthor.add(books.get(i));
            }
        }
        return booksByAuthor;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public ArrayList<Visitor> getVisitors() {
        return visitors;
    }
}
