create table flowers (
id serial primary key,
name_flowers VARCHAR(50),
price_flowers INT
);
select * from flowers;


insert into flowers (name_flowers, price_flowers)
values ('rose', 100);
insert into flowers (name_flowers, price_flowers)
values ('lily', 50);
insert into flowers (name_flowers, price_flowers)
values ('chamomile', 25);


commit ;