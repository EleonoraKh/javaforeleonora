package professional.homework1.homework1_6;

public class Main {
    public static void main(String[] args) throws Exception {
        FormValidator formValidator = new FormValidator();
        formValidator.checkName("Фома");
        formValidator.checkBirthdate("25.12.2021");
        formValidator.checkGender("MALE");
        formValidator.checkHeight("92");

    }

}
