package homework3_3.Task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Competition {
    private List<Dog> dogs = new ArrayList<>();

    private List<Participant> participants = new ArrayList<>();

    public void setParticipants() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            participants.add(new Participant(scanner.next()));
        }

        for (int i = 0; i < n; i++) {
            dogs.add(new Dog(scanner.next()));
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                dogs.get(i).setGrades(scanner.nextInt());
            }
        }
    }

    public void winners() {
        double temp = 0;
        int index = 0;
        for (int i = 0; i < dogs.size(); i++) {
            if (temp < dogs.get(i).averageGrade()) {
                temp = dogs.get(i).averageGrade();
                index = i;
            }
        }

        double temp1 = 0;
        int index1 = 0;
        for (int i = 0; i < dogs.size(); i++) {
            if (temp1 < dogs.get(i).averageGrade() && i != index ) {
                temp1 = dogs.get(i).averageGrade();
                index1 = i;
            }
        }
        double temp2 = 0;
        int index2 = 0;
        for (int i = 0; i < dogs.size(); i++) {
            if (temp2 < dogs.get(i).averageGrade() && i != index && i != index1) {
                temp2 = dogs.get(i).averageGrade();
                index2 = i;
            }
        }
        System.out.println(participants.get(index).getName() + ": " + dogs.get(index).getNick() + ", " + temp);
        System.out.println(participants.get(index1).getName() + ": " + dogs.get(index1).getNick() + ", " + temp1);
        System.out.println(participants.get(index2).getName() + ": " + dogs.get(index2).getNick() + ", " + temp2);
    }
}