package homeWork2_1;

import java.util.Scanner;

/*На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.
Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего.*/
public class Task31 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int x = scanner.nextInt();
        int index = linerSearch(ai, x);
        System.out.println(index + 1);
    }

    public static int linerSearch(int[] ai, int x) {
        int index;
        for (index = 0; index < ai.length; index++) {
            if (ai[index] >= x)
                return index;
        }
        return -1;
    }
}



