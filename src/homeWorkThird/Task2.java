package homeWorkThird;

import java.util.Scanner;

/* На вход подается два положительных числа m и n. Найти сумму чисел между m
и n включительно.
Ограничения:
0 < m, n < 10
m < n*/
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int sum = 0;
        for (int i = m; i <= n; ++i) {
            sum = sum + i;}
                System.out.println(sum);
        }
    }
