package homework3_1.task6;

public class AmazingString {
    private char[] array;

    public AmazingString(char[] array) {
        this.array = array;//присоим весь массив, чот передал
    }

    public AmazingString(String line) {
        array = new char[line.length()];
        for (int i = 0; i < line.length(); i++) {
            array[i] = line.charAt(i);
        }
    }

    public char symbol(int a) {
        return array[a];
    }

    public int dlina() {
        return array.length;
    }

    public void stroka() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();
    }
    public boolean isSubstring(char[] input) {
        int i = 0;

        while (input[0] != array[i]) {
            i++;
            if (i == array.length) {
                return false;
            }
        }

        for (int j = 1; j < array.length - input.length && j < input.length; j++) {
            if (input[j] != array[j + i]) {
                return false;
            }
        }
        return true;
    }
    public boolean isSubstring(String input) {
        char[] fromInput = new char[input.length()];

        for (int i = 0; i < input.length(); i++) {
            fromInput[i] = input.charAt(i);
        }

        int i = 0;

        while (fromInput[0] != array[i]) {
            i++;
            if (i == array.length) {
                return false;
            }
        }

        for (int j = 1; j < array.length - fromInput.length && j < fromInput.length; j++) {
            if (fromInput[j] != array[j + i]) {
                return false;
            }
        }
        return true;
    }

    public void deleteSpaces() {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == ' ') {
                count++;
            } else {
                break;
            }
        }
        char[] array1 = new char[array.length - count];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = array[i + count];
        }
        array = array1;

    }

    public void lineReverse() {
        char[] reverse = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            reverse[i] = array[array.length - 1 - i];
        }
        array = reverse;
    }

    public char[] getArray() {
        return array;
    }
}


