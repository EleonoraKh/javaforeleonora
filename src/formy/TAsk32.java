package formy;

public class TAsk32 {
    public static void main(String[] args) {
        Count myCount = new Count();
        int times = 0;
        for (int i = 0; i < 100; i++)
            increment(myCount, times);
        System.out.println("count равно "
                + myCount.count);
        System.out.println("times равно " + times);
    }

    public static void increment(Count c, int times) {
        c.count++;
        times++;
    }
}

class Count {
   int count;

    Count(int c) {
        count = c;
    }

    Count() {
        count = 1;
    }
}
