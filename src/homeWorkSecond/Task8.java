package homeWorkSecond;

import java.util.Scanner;

/* Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.
Ограничения:
В строке гарантированно есть хотя бы один пробел*/
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String x = scanner.nextLine();
        int k = x.lastIndexOf(' ');
        String first = x.substring(0, k);
        String last = x.substring(k + 1);
        System.out.println(first + '\n' + last);
    }
}
