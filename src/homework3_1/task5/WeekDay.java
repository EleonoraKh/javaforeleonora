package homework3_1.task5;

public enum WeekDay {

    MONDAY(1, "Monday"),
    TUESDAY(2, "Tuesday"),
    WEDNESDAY(3, "Wednesday"),
    THURSDAY(4, "Thursday"),
    FRIDAY(5,"Friday"),
    SATURDAY(6, "Saturday"),
    SUNDAY(7,"Sunday");

    public final int dayNumber;
    public final String name;

    private static final WeekDay[] ALL = values();

    WeekDay(int dayNumber, String name) {
        this.dayNumber = dayNumber;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public static WeekDay byNumber(int dayNumber) {
        for (WeekDay weekDay : ALL) {
            if (weekDay.dayNumber == dayNumber) {
                return weekDay;
            }
        }

        return null;
    }

}
