package homework3_1.task3;

import homework3_1.task2.Student;

import java.util.Arrays;

public class StudentService {
    private StudentService(){

    }
    public static Student bestStudent(Student[] students) {//метод принимает массив студентов, а возвращает 1 студента
        double temp = 0;
        int index = 0;
        for (int i = 0; i < students.length; i++) {
            if (temp < students[i].srednee()) {
                temp = students[i].srednee();
                index = i;
            }
        }
        return students[index];
    }

    public static void sortBySurname(Student[] students) {
        String[] surnames = new String[students.length];
        for (int i = 0; i < students.length; i++) {
            surnames[i] = students[i].getSurname();//взяли итый элемент и вснесли фамилии

        }
        Arrays.sort(surnames);
        Student temp;
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if(surnames[i].equals(students[j].getSurname())){
                    temp = students[i];
                    students[i] = students[j];
                    students[j] = temp;
                    break;
                }

            }

        }

    }
}
