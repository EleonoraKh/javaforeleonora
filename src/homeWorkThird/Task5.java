package homeWorkThird;

import java.util.Scanner;

/*Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения:
0 < m, n < 10

Входные данные Выходные данные
9 1  отв 0
8 3  отв 2
7 9  отв 7*/
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int b;
        b = m % n;
        System.out.println(b);
    }
}
