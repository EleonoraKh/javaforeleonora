package professional.homework1.homework1_3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class FileReadAndWrite {
    private static final String PATH = "/Users/hamanovaeleonora/IdeaProjects/javaforeleonora/" +
            "src/professional/homework1_3";
    private static final String INPUT = "input.txt";
    private static final String OUTPUT = "output.txt";

    public static void main(String[] args) {
        try {
            transformer();

        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void transformer() throws IOException {
        Scanner scanner = new Scanner(new File(PATH + "/" + INPUT));
        Writer writer = new FileWriter(PATH + "/" + OUTPUT);
        String temp = "";
        try (scanner; writer) {
            while (scanner.hasNext()) {
                temp = scanner.nextLine();
                if(temp.matches(".+[A-Za-z]")) {
                    writer.write(temp.toUpperCase() + "\n");
                }
                else {
                    writer.write(temp + "\n");
                }
            }
        }
    }

}
