package homework3_1.task2;

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void newGrade(int grade) {
        for (int i = 0; i < 10; i++) {
            if (grades[i] == 0) {
                grades[i] = grade;
                return;
            }
        }
        for (int i = 0; i < 9; i++) {
            grades[i] = grades[i + 1];
        }
        grades[9] = grade;

    }

    public double srednee() {
        int count = 0;
        double sum = 0;
        for (int i = 0; i < 10; i++) {
            if (grades[i] > 0) {
                count++;
                sum += grades[i];
            }

        }
        return sum / count;


    }


}
