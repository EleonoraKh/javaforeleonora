package homeWork2_1;

import java.util.Scanner;

public class Task41 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int res = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] == arr[i]) {
                res++;
                System.out.println(res + " " + arr[i]);

            }
        }
    }
}

