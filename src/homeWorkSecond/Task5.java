package homeWorkSecond;

import java.util.Scanner;

/* Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
нужно проверить, имеет ли предложенное квадратное уравнение решение или
нет.
На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.*/
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        String str;
        double dis = Math.pow(b, 2) - 4 * a * c;

        if (dis < 0){
            System.out.println("Решения нет");
        }else {
            System.out.println("Решение есть");
        }

    }
}
