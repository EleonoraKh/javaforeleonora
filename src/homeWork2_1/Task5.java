package homeWork2_1;

import java.util.Scanner;

/* На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо.*/
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        for (int j = 0; j < m; j++) {
            int temp = ai[n -1];
            for (int k = n - 1; k > 0; k--)
            {
                ai[k] = ai[k - 1];
            }
            ai[0] = temp;
        }
        for (int k = 0; k < n; k++) {
            System.out.print(ai[k ] + " ");
        }
    }
}
