package weekPracticeFourth;
/*
Начальный вклад в банке равен 1000.
Каждый месяц размер вклада увеличивается на P процентов от имеющейся суммы (0 < P < 25).
Найти через какое количество времени размер вклада будет больше 1100
и вывести найденное количество месяцев и итоговой размер вклада.

15 ->
1
1150.0

3 ->
4
1125.50881
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int p = scanner.nextInt();

    double start = 1000;
    double limit = 1100;
    int month = 0;

    while (start < limit) {
        start += start * p / 100;
        month++;
    }

//        deposit *= (double) (100 + percent) / 100;
//        month++;

//        Scanner scan = new Scanner(System.in);
//        double p = (scan.nextInt()) / 100.00;
//        int i = 0;
//        double sum = 1000;
//        for (; sum <= 1100; sum += sum * p) {
//            i++;
//        }
//        System.out.println(i);
//        System.out.println((int) sum);

    System.out.println(month);
    System.out.println(start);

}

}
