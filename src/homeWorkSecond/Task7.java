package homeWorkSecond;

import java.util.Scanner;

/* Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.*/
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String x = scanner.nextLine();
        int k = x.indexOf(' ');
        String first = x.substring(0, k);
        String last = x.substring(k + 1);
        System.out.println(first + '\n' + last);
    }
}
