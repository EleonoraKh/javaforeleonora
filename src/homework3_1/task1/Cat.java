package homework3_1.task1;

public class Cat {
    private void sleep() {

        System.out.println("sleep");
    }

    private void meow() {

        System.out.println("meow");
    }

    private void eat() {
        System.out.println("eat");
    }

    public void status() {
        int n = (int)(Math.random() * 3);
        switch (n) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }


    }


}


