package homework3_3.task1;

public class Main {
    public static void main(String[] args) {
       Animals bat = new Bat();
       Animals dolphin = new Dolphin();
       Animals goldFish = new GoldFish();
       Animals eagle = new Eagle();
        System.out.println("bat");
        bat.eat();
        bat.sleep();
        bat.birth();
        ((Bat)bat).fly();
        System.out.println("dolphin");
        dolphin.eat();
        dolphin.sleep();
        dolphin.birth();
        ((Dolphin)dolphin).swim();
        System.out.println("goldFish");
        goldFish.eat();
        goldFish.sleep();
        goldFish.birth();
        ((GoldFish)goldFish).swim();
        System.out.println("eagle");
        eagle.eat();
        eagle.sleep();
        eagle.birth();
        ((Eagle)eagle).fly();
    }
}
