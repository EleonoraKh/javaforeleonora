package homeWork2_1;

import java.util.Scanner;

/* На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.*/
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        String[] inputStrings = new String[n];
        for (int i = 0; i < inputStrings.length; i++){
            inputStrings[i] = scanner.next();
        }
        String s = "";
        for (int i = 0; i < n; i++){
            for (int j =0; j < i; j++){
                if(inputStrings[i].equals(inputStrings[j])){
                    s = inputStrings[i];
                }
            }
        }
        System.out.println(s);
    }
}
