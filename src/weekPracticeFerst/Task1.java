package weekPracticeFerst;
import java.util.Scanner;

/*
      Даны числа a, b, c. Нужно перенести значения
   из a -> b, из b -> с, и из c -> а.

   Входные данные:
   a = 3, b = 2, c = 1.
    */

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        System.out.println("Результат ввода а = " + a + " результат ввода b" + b + " результат ввода с " + c);
        int x = a;
        a = b;
        b = c;
        c = x;
        System.out.println("Результат отражения а= " + a + " результат отражения b=" + b + " результат отражения с=" + c);
    }
}
