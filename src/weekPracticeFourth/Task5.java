package weekPracticeFourth;
/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */

import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("Входные данные: n= " + n);
        System.out.println("Выходные данные: ");

        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            System.out.println("SUM in WHILE LOOP: " + sum);

            n = n / 10;
            System.out.println("N in WHILE LOOP: " + n);
        }
        System.out.println("Answer: " + sum);

        //через преобразование в строку:
        /*
        sum += Integer.parseInt(String.valueOf(num.charAt(i)));
         */
    }

}
