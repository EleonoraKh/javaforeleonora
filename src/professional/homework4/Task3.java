package professional.homework4;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        long result = list.stream()
                .filter(s -> !s.isEmpty())
                .count();
        System.out.println(result);
    }
}
