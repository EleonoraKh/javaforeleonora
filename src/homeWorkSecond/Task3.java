package homeWorkSecond;

import java.util.Scanner;

/* (1 балл) Петя снова пошел на работу. С сегодняшнего дня он решил ходить на
обед строго после полудня. Периодически он посматривает на часы (x - час,
который он увидел). Помогите Пете решить, пора ли ему на обед или нет. Если
время больше полудня, то вывести "Пора". Иначе - “Рано”.*/
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str;
        if (n < 13 && n > 0){
            System.out.println("Рано");
        }
        else{
            System.out.println("Пора");
        }
    }
}
