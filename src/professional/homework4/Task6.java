package professional.homework4;

import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        Set<Integer> set1 = Set.of(23, 44, 55);
        Set<Integer> set2 = Set.of(11, 22, 33);
        Set<Set<Integer>> set = Set.of(set1,set2);

        Set<Integer> result = set.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
        System.out.println(result);
    }
}
