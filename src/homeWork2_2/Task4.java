package homeWork2_2;
/*На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
*/
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scan.nextInt();
            }
        }
        int p = scan.nextInt();
        int i1 = 0;
        int j1 = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(a[i][j] == p){
                    i1 = i;
                    j1 = j;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            if (i != i1){
                for (int j = 0; j < n; j++) {
                    if (j != j1) {
                        if (j < n - 1) {
                            System.out.print(a[i][j]);
                            if (j < a.length - 2) {
                                System.out.print(" ");
                            }
                        }else{
                            System.out.print(a[i][j]);
                        }
                    }
                }
                System.out.println();
            }
        }
    }

}
