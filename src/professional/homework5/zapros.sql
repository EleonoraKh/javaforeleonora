select * from orders
join buyers on orders.buyers_id = buyers.id
and orders.id = 5;

select * from orders
where buyers_id = 1
and date_orders >= now() - interval '1 month';

select flowers.name_flowers, orders.number_of_flowers from flowers
join orders on flowers.id = orders.flowers_id
where orders.number_of_flowers = (select max(number_of_flowers)
from orders);

select sum(orders.number_of_flowers * flowers.price_flowers)
from orders, flowers
where orders.flowers_id = flowers.id;