create table buyers (
id serial primary key,
name_client VARCHAR(30) not null,
mobile VARCHAR(30) not null
);
select * from buyers;

insert into buyers(name_client, mobile)
values ('Ivan', '8-914-111-2222');
insert into buyers(name_client, mobile)
values ('Petr', '8-924-111-3333');
insert into buyers(name_client, mobile)
values ('Sergey', '8-924-333-4444');
commit ;
