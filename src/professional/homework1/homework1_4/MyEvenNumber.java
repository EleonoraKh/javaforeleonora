package professional.homework1.homework1_4;

public class MyEvenNumber {
    private final int n;
    public MyEvenNumber(int n){
        if (n % 2 != 0){
            throw new ArithmeticException();
        }
        else {
            this.n = n;
        }
    }
    public int getN(){
        return n;
    }
}
