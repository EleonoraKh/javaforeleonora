package formy;

import java.util.Scanner;

public class Proekt {
    static final double ROUBLES_PER_EURO = 72.12;
    public static void main(String[] args) {


        int[] euroArray;
        double[] roublesArray;

        int n;
        int i;

        Scanner input = new Scanner(System.in);
        //Отобразить инструкцию
        instruct();

        // Получить количество конвертаций до тех пор,
        // пока не введено корректное значение
        do {
            System.out.println("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        }while (n <= 0);

        //Получить n сумм денег в евро
        System.out.print("Введите  " +n+ " сумм денег "
        + "в евро через пробел: ");
        euroArray = new int[n];
        for (i =0; i < n; ++i)
            euroArray[i] = input.nextInt();

        // Конвертировать сумму  денег в российские рубли
        roublesArray = find_roubles(euroArray, n);

         // Отобразить в таблице n сумм денег в евро и эквивалентные
        // им суммы денег в российских рублях в пользу покупателя
         System.out.println("\n   Сумма, ₽  Сумма, $");
         for (i = 0; i < n; ++i)
             System.out.println("\t " + euroArray[i] + "\t"
             + (int)(roublesArray[i] * 100) / 100.0);
    }
    /**
     * Отображает инструкцию
     */
    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег "
                        + "из евро в российские рубли.");
        System.out.println("Курс покупки равен " + ROUBLES_PER_EURO + " рубля.\n");
    }
    /**
     * Конвертирует сумму денег из евро в российские рубли
     */
    public static double [] find_roubles(int[] euroArray, int n){
        double[] roublesArray = new double[n];
        int i;

        for (i = 0; i < n; ++i)
            roublesArray[i] = ROUBLES_PER_EURO * euroArray[i];

        return roublesArray;

    }
}
