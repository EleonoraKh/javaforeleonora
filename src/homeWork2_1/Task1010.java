package homeWork2_1;

import java.util.Scanner;

public class Task1010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = (int)(Math.random() * 1001);
        System.out.print("Введите число от 0 до 1000: ");
        int n = scanner.nextInt();
        while (n != number){
            if(n < 0){
                System.out.println("Игра завершена");
                return;
            }
            if(n < number) {
                System.out.println("Это число меньше загаданного.");

            }
            if (n > number){
                System.out.println("Это число ,большее загаданного.");
            }
            System.out.print("Введите число от 0 до 1000: ");
             n = scanner.nextInt();
        }
        System.out.println("Победа!");

    }
}

