package homeWorkThird;
/* На вход подается два положительных числа m и n. Необходимо вычислить m^1
+ m^2 + ... + m^n*/
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int i = 1;
        int d =0;
        for ( i = 1; i <= n; i++) {
            d +=(int)Math.pow(m, i);
        }
        System.out.println(d);
    }
}