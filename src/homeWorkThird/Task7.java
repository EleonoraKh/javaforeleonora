package homeWorkThird;

import java.util.Scanner;

/*Дана строка s. Вычислить количество символов в ней, не считая пробелов
        (необходимо использовать цикл).
        Ограничения:
        0 < s.length() < 1000*/
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') {
                result++;
            }
        }
        System.out.println(result);
    }
}
