package homeWork2_1;

import java.util.Scanner;

/*На вход подается строка S, состоящая только из русских заглавных
букв (без Ё).
Необходимо реализовать метод, который кодирует переданную строку с
помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
нужно пробелом.
Java 12 Базовый модуль Неделя 4
ДЗ 2 Часть 1
Для удобства представлен массив с кодами Морзе ниже:
{".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
"...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"}*/
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        codeToMorze(s);
    }
    public static void codeToMorze(String m) {
        String s = "АБВГДЕЖЗИЙКЛMНОПРСТУФХЦЧШЩЪЬЫЭЮЯ";
        String[] arr = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
            for (int j = 0; j < m.length(); j++){
                for(int i = 0; i < arr.length; i++){
                if(s.charAt(i) == m.charAt(j)){
                    System.out.print(arr[i]+ " ");
                    break;
                }
            }
        }

    }

}
