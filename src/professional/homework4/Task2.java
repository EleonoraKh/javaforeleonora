package professional.homework4;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        int result = list.stream()
                .reduce(1, (x1,x2) -> x1 * x2);
        System.out.println(result);
    }
}
