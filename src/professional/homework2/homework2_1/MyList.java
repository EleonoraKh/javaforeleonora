package professional.homework2.homework2_1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyList {
    public static void main(String[] args) {
        List<String> list1 = List.of("mam","dad","mam","sun");
        System.out.println(unic(list1));
        List<Integer> list2 = List.of(1, 23, 55, 23);
        System.out.println(unic(list2));
    }
    private static <T> Set<T> unic(List<T> list) {
        return new HashSet<>(list);
    }
}
