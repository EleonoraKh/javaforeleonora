package professional.homework4;

import java.util.List;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        list.stream()
                .map(s -> s.toUpperCase())
                .forEach(s -> System.out.print(s + ", " ));
    }
}
