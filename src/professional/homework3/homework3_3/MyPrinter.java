package professional.homework3.homework3_3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyPrinter {
    public static void main(String[] args) {
       Class<APrinter> cls = APrinter.class;
       try {
           Class[] params = new Class[]{int.class};
           Method method = cls.getMethod("print", params);
           method.invoke(cls.newInstance(), 5);
       }
       catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | IllegalArgumentException |
       InstantiationException e) {
           System.out.println(e.getMessage());
       }
    }
}
