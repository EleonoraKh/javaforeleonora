package homeWork2_1;

import java.util.Scanner;

public class Task32 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int x = scanner.nextInt();
        System.out.println(binarySearch(ai , x));
    }

    public static int binarySearch(int[] ai, int x) {
        int low = 0;
        int high = ai.length - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (x < ai[mid])
                high = mid - 1;
            else if (x ==ai[mid])
                return mid;
            else if (ai[mid +1] > x)
                return mid;
            else
                low = mid + 1;
        }
        return low; // Теперь high < low
    }
}

