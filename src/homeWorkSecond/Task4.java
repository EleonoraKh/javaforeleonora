package homeWorkSecond;

import java.util.Scanner;

/* После вкусного обеда Петя принимается за подсчет дней до выходных.
Календаря под рукой не оказалось, а если спросить у коллеги Феди, то тот
называет только порядковый номер дня недели, что не очень удобно. Поэтому
Петя решил написать программу, которая по порядковому номеру дня недели
выводит сколько осталось дней до субботы. А если же сегодня шестой
(суббота) или седьмой (воскресенье) день, то программа выводит "Ура,
выходные!"*/
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str;
        int x = 6 - n;
        if (x == 0 || x == -1) {
            System.out.println("Ура, выходные!");
        }else{
            System.out.println(x);
        }
    }
}
