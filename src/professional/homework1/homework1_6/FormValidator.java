package professional.homework1.homework1_6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {
    public void checkName(String str) throws Exception {
        if(!str.matches("^[А-Я][а-я]{1,19}$")){
            throw new Exception();
        }
        else {
            System.out.println("Имя введено верно");
        }
    }

    public void checkBirthdate(String str) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate birthday = LocalDate.parse(str, formatter);
        LocalDate begin = LocalDate.parse("01.01.1900", formatter);
        LocalDate finish = LocalDate.now();
        if(!(birthday.isBefore(finish) && birthday.isAfter(begin))) {
            throw new Exception();
        }
        else {
            System.out.println("День рождения введен верно");
        }
    }

    public void checkGender(String str) throws IllegalArgumentException {
        Gender.valueOf(str);
    }

    public void checkHeight(String str){
        try {
            double height = Double.parseDouble(str);
            if(height <= 0) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
