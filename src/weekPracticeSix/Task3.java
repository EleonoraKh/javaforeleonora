package weekPracticeSix;
/*
Развернуть строку рекурсивно.

abcde -> edcba
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(reverseString(s));

    }

    public static String reverseString(String s) {
        if (s.isEmpty()) {
            return s;
        } else {
            System.out.println("s.charAt(0)= " + s.charAt(0));
            System.out.println("s.substring(1)=" + s.substring(1));

            return reverseString(s.substring(1)) + s.charAt(0);
            // в StringBuilder есть функция по переворачииванию фраз

        }

    }
}

