create table orders(
id serial primary key,
buyers_id int references buyers(id),
flowers_id int references flowers(id),
number_of_flowers int not null,
date_orders timestamp
);
select * from orders;
insert into orders(buyers_id, flowers_id, number_of_flowers, date_orders)
values (1, 1, 51, now());
insert into orders(buyers_id, flowers_id, number_of_flowers, date_orders)
values (2, 3, 101, now());
insert into orders(buyers_id, flowers_id, number_of_flowers, date_orders)
values (3, 2, 99, now());
insert into orders(buyers_id, flowers_id, number_of_flowers, date_orders)
values (1, 3, 7, now());
insert into orders(buyers_id, flowers_id, number_of_flowers, date_orders)
values (1, 1, 11, now());
insert into orders(buyers_id, flowers_id, number_of_flowers, date_orders)
values (2, 2, 11, now());


commit;
