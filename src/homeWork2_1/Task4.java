package homeWork2_1;

import java.util.*;

/*На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.*/
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        int [] numbers = new int[n];
        numbers[0] = ai[0];
        int ind = 1;
        for (int i = 1; i < n; i++) {
            if ((ai[i] != ai[i - 1]) && (ai[i] != ai[0])) {
                numbers[ind] = ai[i];
                ind++;
            }
        }

        int [] counter = new int[n];
        int index = 0;
        for (int i = 0; i < n; ) {
            int j = 1;
            while(++i < n && ai[i] == ai[i - 1]) {
                ++j;
            }
            counter[index] = j;
            index++;
        }

        for (int m = 0; m < numbers.length; m++) {
            if(counter[m] != 0)
            {
                System.out.println( counter[m] + " " +  numbers[m] + " ");
            }

        }
    }
}











