package homeWork2_1;

import java.util.Scanner;

/*На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.*/
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] number = new double[n];

        //заполняем наш массив входными данными
        for (int i = 0; i < n; i++) {
            number[i] = scanner.nextDouble();
        }
        double avarage = 0;
        if (number.length > 0) ;
        {
            double result = 0;
            for (int j = 0; j < number.length; j++) {
                result += number[j];
            }
            avarage = result / number.length;
            System.out.println(avarage);
        }
    }
}
