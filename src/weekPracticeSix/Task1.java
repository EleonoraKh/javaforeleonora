package weekPracticeSix;
/*
Найдем факториал числа n рекурсивно.
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        int res = 1;
//        for (int i = 1; i <= n; i++) {
//            res = res * i;
//        }
//        System.out.println("Factorial: " + res);
        // via - это через

        int res = factorial(n);
        System.out.println("Factorial via recursion: " + res);
    }

    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }
}
