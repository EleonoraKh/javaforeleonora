package homeWork2_2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(number(n));
    }

    public static String number(int n) {
        if(n < 10){
            return "" + n;
        }
        else {
            return n % 10 + " " + number(n / 10);
        }

    }
}
