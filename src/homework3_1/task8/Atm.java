package homework3_1.task8;

public class Atm {
    private double kurs;

    //присваем статическую переменную, счетчик банкоматов
    private static int count = 0;
//конструктор
    public Atm(double rub, double dollar) {
        kurs = rub / dollar;
        count++;
    }

    public double dollarToRub(int sum) {

        return  sum * kurs;
    }

    public double rubToDollar(int sum) {
        return sum / kurs;
    }
    public static int getCount(){
        return count;
    }

}
