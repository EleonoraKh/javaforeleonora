package homeWork2_2;

import java.util.Scanner;

/*На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.*/
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] mas = new int[n][n];
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();
        // бегаем сканируем массив
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                //меняем массив
                if ((i == y1 || i == y2) && (j <= x2 && j >= x1)) {

                    mas[i][j] = 1;
                } else if ((j == x1 || j == x2) && (i >= y1 && i <= y2)) {
                    mas[i][j] = 1;

                }
            }

        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {



                //если индекс столбца не последний(j != n - 1 ?),
                // то печатаем элемент массива с пробелом : игначе печатаем элемент без пробела
                System.out.print(j != n - 1 ? mas[i][j] + " " : mas[i][j]);

            }
            System.out.println();
        }
    }
}





