package homeWork2_2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] norma = new int[4];
        for (int i = 0; i < 4; i++) {
            norma[i] = scanner.nextInt();
        }
        int[][] s = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                s[i][j] = scanner.nextInt();
            }
        }
        int[] sums = new int[4];
        int sum = 0;
        for (int i = 0; i < 4; i++) {
            // сбрасываем, что бы каждый раз сумма считалась по новой по столбикам отдельно
            sum = 0;
            for (int j = 0; j < 7; j++) {
                sum += s[j][i];

            }
            //присвоили суммированные столбики каждому элементу значения sums
            sums[i] = sum;

        }
        for (int i = 0; i < 4; i++) {
            if(sums[i] <= norma[i]){
                continue;

            }
            else {
                System.out.println("Нужно есть поменьше");
                return;
            }
        }
        System.out.println("Отлично");



    }
}
