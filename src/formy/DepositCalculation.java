package formy;
import java.time.LocalDate;
import java.util.Scanner;

public class DepositCalculation {
    static final int MONTHS_OF_YEAR = 12; //месяцев в году
    static final int DAYS_YEAR = 365; //дней в году

    public static void main(String[] args) {
        int startMonth; // номер месяца открытия вклада
        int startYear; // год открытия вклада
        int depositTerm; // срок вклада в месяцах
        double rate; // процентная ставка (% годовых)
        double depositAmount; // сумма вклада
        double accruedInterestPerMonth;
        double accruedInterestTotal = 0;

        Scanner input = new Scanner(System.in);

        //получить номер месяца открытия вклада
        System.out.println("Введите номер месяца открытия вклада: ");
        startMonth = input.nextInt();

        //получит год открытия вклада
        System.out.println("Введите год открытия вклада: ");
        startYear = input.nextInt();

        //получит срок вклада в меясцах
        System.out.println("Введите срокя вклада в месяцах: ");
        depositTerm = input.nextInt();

        //получить сумму вклада
        System.out.println("Введите сумму вклада: ");
        depositAmount = input.nextInt();

        //получить годовую процентную ставку
        System.out.println("Введите годовую процентную ставку: ");
        rate = input.nextDouble();

        int monthsCount = 0; //счетчик месяцев
        int i = startYear;
        while (monthsCount < depositTerm) {

            for (int j = 1; j <= MONTHS_OF_YEAR; j++) {
                //Если номер месяца меньше или равно номеру месяца открытия вклада
                //то переходим к следующему месяцу, он не попадает в интервал
                if (i == startYear && j <= startMonth) {
                    continue;
                }
                //Если номер текущего месяца равен сроку вклада,
                //то расчет заканчивается и выходим из цикла
                if (monthsCount == depositTerm) {
                    break;
                }
                //Получаем количество дней в текущем месяце
                int lenghtOfMounth = LocalDate.of(i, j, 1).lengthOfMonth();

                //Рассчитаем доход по вкладу в текущем месяце
                accruedInterestPerMonth = depositAmount * rate / 100 / DAYS_YEAR * lenghtOfMounth;
                accruedInterestPerMonth = Math.round(accruedInterestPerMonth * 100) / 100.0; // округление до копейки

                //Вывести результат начисленных процентов в месяце
                System.out.println("Начислено процентов в " + i + " году " + j + " месяца= " + accruedInterestPerMonth);

                //Добавим процент по вкладам текущего месяца  к суммарной выплате
                accruedInterestTotal = accruedInterestTotal + accruedInterestPerMonth;
                monthsCount++; // увеличим счетчик месяцев на 1

            }
            i++; //увеличим год на 1
        }
        //Вывести итоговый результат начисленяи процентов
        System.out.println("Всего начислено процентов= " + accruedInterestTotal);
    }

}



