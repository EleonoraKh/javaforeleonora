package homeWork2_2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                mas[i][j] = scanner.nextInt();
            }
        }
        //сравнили координаты на сим по диаг, иф проверили условие, что коорд симм, елс либо нет, то фолс.
        // cont  мы даем понять, что двигаемся дальше, return; выход из метода
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (mas[i][j] == mas[n - 1 - j][n - 1 - i]) {
                    continue;
                } else {
                    System.out.println(false);
                    return;
                }
            }
        }
        System.out.println(true);

    }
}
