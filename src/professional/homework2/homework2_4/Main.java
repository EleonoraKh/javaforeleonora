package professional.homework2.homework2_4;


import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Document doc1 = new Document(1, "Книга АС Пушкин", 50);
        Document doc2 = new Document(2, "Книга Есенин", 1);

        List<Document> docs = Arrays.asList(doc1, doc2);
        DocumentOrganizer res = new DocumentOrganizer();
        System.out.println(res.organizeDocuments(docs));
    }
}
