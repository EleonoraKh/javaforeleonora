package formy;

public class Task26 {
    public static void main(String[] args) {

    }
    public static boolean isPalindrome(String s) {
        return isPalindrome(s, 0, s.length() - 1);
    }
    public static boolean isPalindrome(String s, int low, int high){
        if (high <= low) // простой случай
            return true;
        else if (s.charAt(low) != s.charAt(high)) //прост случай
            return false;
        else
            return isPalindrome(s, low, high);

        }

    }

