package homework3_3.task2;

public class Main {
    public static void main(String[] args) {
        Furniture a = new Stool();
        Furniture b = new Cupboard();
        Furniture c = new Furniture();
        Furniture d = new Table();
        System.out.println(BestCarpenterEver.isStool(a));
        System.out.println(BestCarpenterEver.isStool(b));
        System.out.println(BestCarpenterEver.isStool(c));
        System.out.println(BestCarpenterEver.isStool(d));
    }
}
