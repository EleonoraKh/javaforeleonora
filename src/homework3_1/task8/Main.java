package homework3_1.task8;

public class Main {
    public static void main(String[] args) {
        Atm atm = new Atm(60, 1);
        System.out.println(atm.dollarToRub(100));
        System.out.println(atm.rubToDollar(6000));
        System.out.println(Atm.getCount());
    }
}
