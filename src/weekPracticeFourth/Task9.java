package weekPracticeFourth;
import java.util.Scanner;

/*
 Дана последовательность из n целых чисел, которая может начинаться с
    отрицательного числа. Определить, какое количество отрицательных чисел
    записано в начале последовательности и прекратить выполнение программы
    при получении первого неотрицательного числа на вход.
Входные данные:
-1
-2
4
Выходные данные:
Result: 2
 */
public class Task9 {
    public static void main(String[] args) {
        //1 case:
//        Scanner scanner = new Scanner(System.in);
//        int result = 0;
//        while (true) {
//            if (scanner.nextInt() < 0)
//                result++;
//            else
//                break;
//        }
//        System.out.println(result);

        //2 case:
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        for (int i = scanner.nextInt(); i < 0; i = scanner.nextInt()) {
            count++;
        }
        System.out.println("result: " + count);
    }
}
