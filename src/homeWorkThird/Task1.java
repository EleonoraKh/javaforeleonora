package homeWorkThird;

import java.io.PrintStream;
import java.util.Scanner;

/* Напечатать таблицу умножения от 1 до 9. Входных данных нет. Многоточие в
примере ниже подразумевает вывод таблицы умножения и для остальных чисел
2, 3 и т. д.*/
public class Task1 {
    public static void main(String[] args) {
        for (int i = 1; i < 11; i++) {
            for (int j = 1; j < 11; j++) {
                 System.out.println(i + " * " + j + " = " + (i * j));
            }
            System.out.println();
        }
    }
}
