package homework3_2.task1_4;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        library.addBook(new Book("Харуки Мураками", "Норвежский лес"));
        library.addBook(new Book("Харуки Мураками", "Норвежский"));
        library.addBook(new Book("Чад Мень Тан", "Ищи в себе"));
        library.addBook(new Book("Александр Пушкин", "Золотая рыбка"));
        library.addBook(new Book("Харуки Мураками", "Норвежский"));

        for (int i = 0; i < library.getBooks().size(); i++) {
            System.out.println(library.getBooks().get(i).getAuthor() + " " + library.getBooks().get(i).getTitle());
        }
        System.out.println();

        library.deleteBook("Норвежский");
        library.deleteBook("Весна");
        library.getBook("Золотая рыбка");
        library.getBook("рыбка");

        for (int i = 0; i < library.getBooks().size(); i++) {
            System.out.println(library.getBooks().get(i).getAuthor() + " " + library.getBooks().get(i).getTitle());
        }
        System.out.println();

        ArrayList<Book> list = library.getBookList("Харуки Мураками");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getAuthor() + " " + list.get(i).getTitle());
        }
    }
}
