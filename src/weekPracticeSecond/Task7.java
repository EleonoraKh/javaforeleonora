package weekPracticeSecond;
/* Реализовать System.out.println(), используя System.out.print() и табуляцию \n Входные данные:
два слова, считываемые из консоли  Входные данные Hello World Выходные данные Hello World
*/
import java.util.Scanner;
public class Task7 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String first = scanner.next();
        String second = scanner.next();

        System.out.print(first + "\n" + second);
    }
}
