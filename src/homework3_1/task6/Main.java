package homework3_1.task6;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        AmazingString srt = new AmazingString(new char[]{' ', 'a', 'b', 'c', 'd'});
        System.out.println(srt.symbol(4));
        System.out.println(srt.dlina());
        srt.stroka();
        System.out.println(srt.isSubstring(new char[]{'c', 'd'}));
        System.out.println(srt.isSubstring(" ab"));
        srt.deleteSpaces();
        System.out.println(Arrays.toString(srt.getArray()));
        srt.lineReverse();
        System.out.println(Arrays.toString(srt.getArray()));
    }
}
