package professional.homework2.homework2_2;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s  = scanner.nextLine();
        String t  = scanner.nextLine();
        System.out.println(isAnagram(s, t));
    }
    private static boolean isAnagram(String s, String t) {
        return Arrays.equals(s.toLowerCase().chars().sorted().toArray(), t.toLowerCase().chars().sorted().toArray());
    }
}
