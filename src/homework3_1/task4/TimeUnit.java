package homework3_1.task4;

public class TimeUnit {
    int hour;
    int minutes;
    int seconds;
    public TimeUnit(int hour){
        if (hour < 0 || hour > 23){
            System.out.println("Введите корректное значение часов: ");
            System.exit (1);
        }
        this.hour = hour;
        this.minutes = 0;
        this.seconds = 0;

    }

    public TimeUnit(int hour, int minutes){
        this(hour);
        if (minutes < 0 || minutes > 59) {
            System.out.println("Введите корректное значение минут: ");
            System.exit (1);
        }
        this.minutes = minutes;

    }

    public TimeUnit(int hour, int minutes, int seconds) {
        this(hour, minutes);
        if (seconds < 0 || seconds > 59){
            System.out.println("Введите корректное значение секунд: ");
            System.exit (1);
        }

        this.seconds = seconds;

    }

    public void time24(){
        if(hour < 10){
            System.out.print("0" + hour + ":");
        }
        else {
            System.out.print(hour + ":");
        }
        if(minutes < 10){
            System.out.print("0" + minutes + ":");
        }
        else {
            System.out.print(minutes + ":");
        }
        if(seconds < 10){
            System.out.println("0" + seconds);
        }
        else {
            System.out.println(seconds);
        }

    }
    public void time12(){
        if(hour >= 0 && hour < 12){
            if(hour < 10 && hour > 0){
                System.out.print("0" + hour + ":");
            }
            else if (hour == 0) {
                System.out.print(12 + ":");

            } else {
                System.out.print(hour + ":");
            }
            if(minutes < 10){
                System.out.print("0" + minutes + ":");
            }
            else {
                System.out.print(minutes + ":");
            }
            if(seconds < 10){
                System.out.println("0" + seconds + " am");
            }
            else {
                System.out.println(seconds + " am");

            }

        }

        if (hour >= 12 && hour < 24){
            int temp = hour - 12;
            if(temp < 10 && temp > 0){
                System.out.print("0" + temp + ":");
            }
            else if (temp == 0) {
                System.out.print(12 + ":");

            } else {
                System.out.print(temp + ":");
            }
            if(minutes < 10){
                System.out.print("0" + minutes + ":");
            }
            else {
                System.out.print(minutes + ":");
            }
            if(seconds < 10){
                System.out.println("0" + seconds + " pm");
            }
            else {
                System.out.println(seconds + " pm");
            }
        }
    }
    public void addTime(int hour, int minutes, int seconds){
        this.hour += hour;
        this.minutes += minutes;
        this.seconds += seconds;
        if(this.seconds > 59){
            this.minutes += this.seconds / 60;
            this.seconds %= 60;
        }

        if(this.minutes > 59){
            this.hour += this.minutes / 60;
            this.minutes %=60;
        }

        if(this.hour > 23){
            this.hour %= 24;

        }
    }

}
