package formy;

public class Task25 {
    public static void main(String[] args) {
        System.out.println(
                "Сумма равна " + xMethod(4));
    }
     static int xMethod(int n) {
        if (n == 1)
            return 1;
        else
            return n + xMethod(n - 1);
    }
}