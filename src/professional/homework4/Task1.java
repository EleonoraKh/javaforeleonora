package professional.homework4;

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        int result = IntStream.rangeClosed(1,100)
                .filter(n -> n%2 == 0)
                .sum();
        System.out.println(result);
    }
}
