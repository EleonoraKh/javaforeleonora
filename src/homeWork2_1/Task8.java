package homeWork2_1;

import java.util.Scanner;

/* На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.*/
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < ai.length; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int near = closest(m, ai);
        System.out.println(near);
    }

    public static int closest(int m, int[] ai) {
        int min = Integer.MAX_VALUE;
        int closest = m;

        for (int v : ai) {
         //   System.out.println("v " + v + " min " + min);
            final int diff = Math.abs(v - m);
         //  System.out.println("diff " + diff);
            if (diff <= min) {
                min = diff;
          //      System.out.println("min " + min);
                closest = v;
                if (closest > v) {
                    closest = v;
                }
           //     System.out.println("closest " + closest);
            }
        }

        return closest;
    }
}





