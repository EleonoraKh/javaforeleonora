package professional.homework1.homework1_2;

public class MyUncheckedException extends ArithmeticException {
    public MyUncheckedException(){
        super();
    }
    public MyUncheckedException(String message){
        super(message);
    }
}
