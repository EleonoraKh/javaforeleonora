package homeWorkThird;

import java.util.Scanner;

/*В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
Ограничения:
0 < n < 1000000
 */
public class Task6 {
    public static void main(String[] args) {
        int d8, d4, d2, d1;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        d8 = n / 8;
        n %= 8;
        d4 = n / 4;
        n %=4;
        d2 = n / 2;
        n %= 2;
        d1 = n/1;
        n %= 1;

        System.out.println(d8 + " "+ d4+ " "+ d2+" " + d1);
    }
}
